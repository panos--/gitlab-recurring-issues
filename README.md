# gitlab recurring issues

gitlab_recurring issue is a simple bot for gitlab.
It looks at specific tags attached to issues and re-creates (duplicates) issues
periodically.

This bot is aimed to go around https://gitlab.com/gitlab-org/gitlab/-/issues/15981
and extend the idea described here: https://gitlab.com/gitlab-org/gitlab/-/issues/15981#note_214875348

It uses gitlab CI to periodycally run the bot, looking for project issues and
duplicates issues when necessary.

By default, the bot closes the old issue.

Issue unicity check is done on the title.

(so if you create an issue "task 1" with periodicity and "task 2" with periodicity, the
bot will duplicate both issues)

## usage

To use the bot, you must use the following tags:

- recurring::hourly
- recurring::daily
- recurring::weekly
- recurring::monthly
- recurring::yearly
- recurring::schedule

and attach one of these tags to a project issue.

Then create a gitlab CI for the project, example with a simple one:


```
image: "python:latest"

before_script:
 - pip install gitlab-recurring-issues


stages:
  - bot


recurring-issues:
  stage: bot
  script:
    - recurring_issues --private-token
```

you must set in project settings / CI/CD a variable named: GITLAB_PRIVATE_TOKEN
filled with a token authorized to update the project via api calls.

and then schedule a CI/CD trigger every X depending of your configuration
(every hour, day, etc..)

The bot will be run every period and will recreate issues when necessary.

The date used to decide if we need to create a new issue or not is the issue creation_date

the bot creates a new issue and duplicates the following data from the old one:

- title
- description
- assignee
- tags (including recuring tag)
- weight
- confidential
- if there is a due date in the source issue, looks for interval between source
  creation date and source due date and add a due date for the new issue
  with same interval


### advanded usage

#### using cron-like schedules

Cron-like schedules are supported with the tag `recurring::schedule`. The actual
schedule has to be specified in an issue's
[front matter](https://docs.gitlab.com/ee/user/markdown.html#front-matter) with
a property named `schedule`. Only YAML-formatted front matter is supported
though.

The schedule has to be specified in a format supported by
[crontab](https://pypi.org/project/crontab/).

##### Schedule Example

```yaml
---
schedule: '0 6 15 */3 * *'
---

# Issue Content

...
```

An issue with this content, tagged `recurring::schedule` will be re-scheduled
on the 15th of every third month around 6 a.m., depending on the run frequency
of the bot.

#### do not close previous issue

By default the bot close the previous same issue if not already closed when creating
the new one.
(This is a common behaviour for recurring tasks like "check logs" or so: no need to keep
older same tasks)

If you want to keep the old task, add a new label to the last issue:

**recurring:do-not-close-previous**

##### stop creating new issues

the bot will always create new issues, even if the previous one is closed.
If you remove the label for the previous one, the bot will look at the issue before.
In this case, you will need to remove all labels "recurring::xx" to stop creating new issues
for a given title.

To avoid editing all old issues, just add the tag: **recurring:stop** to the last issue
and the bot will stop creating new issues for this title.

If you wan to reactivate issue creation:
- either create a new issue with the same title and add "recurring::xx" periodicity
- or remove the "recurring:stop" tag of the last issue
